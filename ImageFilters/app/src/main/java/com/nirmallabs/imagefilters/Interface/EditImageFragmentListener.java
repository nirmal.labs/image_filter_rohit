package com.nirmallabs.imagefilters.Interface;

public interface EditImageFragmentListener {
    void onBrightnessChanged(int brightness);
    void onSaturationChanged(float saturation);
    void onConstrantChanged(float constraint);
    void onEditComplete();
    void onEditStarted();
}
