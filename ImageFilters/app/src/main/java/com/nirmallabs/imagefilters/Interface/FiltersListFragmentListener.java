package com.nirmallabs.imagefilters.Interface;


import com.zomato.photofilters.imageprocessors.Filter;

public interface FiltersListFragmentListener {
    void onFilterSelected(Filter filter);

}
