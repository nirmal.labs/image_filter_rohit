package com.nirmallabs.imagefilters;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nirmallabs.imagefilters.Adapter.ViewPagerAdapter;
import com.nirmallabs.imagefilters.Interface.EditImageFragmentListener;
import com.nirmallabs.imagefilters.Interface.FiltersListFragmentListener;
import com.nirmallabs.imagefilters.Utils.BitmapUtils;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.imageprocessors.subfilters.BrightnessSubFilter;
import com.zomato.photofilters.imageprocessors.subfilters.ContrastSubFilter;
import com.zomato.photofilters.imageprocessors.subfilters.SaturationSubFilter;

import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity implements FiltersListFragmentListener, EditImageFragmentListener {

    public static final String picture_name="flash.jpg";
    public static final int PERMISSION_PIC_IMAGE=1000;

    ImageView img_preview;
    TabLayout tabLayout;
    ViewPager viewPager;
    CoordinatorLayout coordinatorLayout;
    Bitmap originalBitmap,filteredBitmap,finalBitmap;
    EditImageFragment editImageFragment;
    FiltersListFragment filtersListFragment;

    int brightnessFinal=0;
    float saturationFinal=1.0f;
    float constrantFinal=1.0f;

    static {
        System.loadLibrary("NativeImageProcessor");
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar=findViewById(R.id.toolBar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("Filter");

//        img_preview=findViewById(R.id.image_preview);
//        tabLayout=findViewById(R.id.tabs);
//        viewPager=findViewById(R.id.viewpager);
        coordinatorLayout=findViewById(R.id.coordinator);

        loadImage();

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void loadImage() {
        originalBitmap= BitmapUtils.getBitmapFromAssets(this,picture_name,300,300);
        filteredBitmap=originalBitmap.copy(Bitmap.Config.ARGB_8888,true);
        img_preview.setImageBitmap(originalBitmap);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter=new ViewPagerAdapter(getSupportFragmentManager(),0);
         filtersListFragment=new FiltersListFragment();
        filtersListFragment.setListener(this);

         editImageFragment=new EditImageFragment();
         editImageFragment.setListener(this);

         adapter.addFragment(filtersListFragment,"FILTERS");
         adapter.addFragment(editImageFragment,"EDIT");
         viewPager.setAdapter(adapter);


    }

    @Override
    public void onBrightnessChanged(int brightness) {
        brightnessFinal=brightness;
        Filter myFilter=new Filter();
        myFilter.addSubFilter(new BrightnessSubFilter(brightness));
        img_preview.setImageBitmap(myFilter.processFilter(finalBitmap.copy(Bitmap.Config.ARGB_8888,true)));


    }

    @Override
    public void onSaturationChanged(float saturation) {
        saturationFinal=saturation;
        Filter myFilter=new Filter();
        myFilter.addSubFilter(new SaturationSubFilter(saturation));
        img_preview.setImageBitmap(myFilter.processFilter(finalBitmap.copy(Bitmap.Config.ARGB_8888,true)));


    }

    @Override
    public void onConstrantChanged(float constraint) {
        constrantFinal=constraint;
        Filter myFilter=new Filter();
        myFilter.addSubFilter(new ContrastSubFilter(constraint));
        img_preview.setImageBitmap(myFilter.processFilter(finalBitmap.copy(Bitmap.Config.ARGB_8888,true)));


    }

    @Override
    public void onEditComplete() {
        Bitmap bitmap=filteredBitmap.copy(Bitmap.Config.ARGB_8888,true);
        Filter myFilter=new Filter();
        myFilter.addSubFilter(new BrightnessSubFilter(brightnessFinal));
        myFilter.addSubFilter(new SaturationSubFilter(saturationFinal));
        myFilter.addSubFilter(new ContrastSubFilter(constrantFinal));
        finalBitmap=myFilter.processFilter(bitmap);
    }

    @Override
    public void onEditStarted() {

    }

    @Override
    public void onFilterSelected(Filter filter) {
        resetControl();
        finalBitmap=originalBitmap.copy(Bitmap.Config.ARGB_8888,true);
        img_preview.setImageBitmap(filter.processFilter(finalBitmap));
        finalBitmap=filteredBitmap.copy(Bitmap.Config.ARGB_8888,true);

    }

    private void resetControl() {
        if(editImageFragment!=null)
            editImageFragment.resetControl();
        brightnessFinal=0;
        saturationFinal=1.0f;
        constrantFinal=1.0f;

    }

    @Override
    public boolean onCreatePanelMenu(int featureId, @NonNull Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_open)
        {
            openImageFromGallary();
            return true;
        }
        if (id==R.id.action_save)
        {
            saveImageToGallary();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveImageToGallary() {
        Dexter.withActivity(this).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted())
                {
                    try {
                        final String path=BitmapUtils.insertImage(getContentResolver(),finalBitmap,
                                System.currentTimeMillis()+"_profile.jpg",null);

                        if (!TextUtils.isEmpty(path))
                        {
                            Snackbar snackbar=Snackbar.make(coordinatorLayout,"Image save to gallary!",Snackbar.LENGTH_LONG)
                                    .setAction("OPEN", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            openImage(path);
                                        }
                                    });
                            snackbar.show();
                        }
                        else
                        {
                            Snackbar snackbar=Snackbar.make(coordinatorLayout,"Unable to save image!",Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    Toast.makeText(MainActivity.this,"Permission Denied",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
             token.continuePermissionRequest();
            }
        }).check();

    }

    private void openImage(String path) {
        Intent intent=new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse(path),"image/*");
        startActivity(intent);
    }

    private void openImageFromGallary() {
        Dexter.withActivity(this).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted())
                {
                    Intent intent=new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    startActivityForResult(intent,PERMISSION_PIC_IMAGE);
                }
                else
                {
                    Toast.makeText(MainActivity.this,"Permition denied!",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
            token.continuePermissionRequest();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_OK && requestCode == PERMISSION_PIC_IMAGE) {
            Bitmap bitmap = BitmapUtils.getBitmapFromGallary(this, data.getData(), 800, 800);
            originalBitmap.recycle();

            originalBitmap=bitmap.copy(Bitmap.Config.ARGB_8888,true);
            finalBitmap=originalBitmap.copy(Bitmap.Config.ARGB_8888,true);
            filteredBitmap=originalBitmap.copy(Bitmap.Config.ARGB_8888,true);
            img_preview.setImageBitmap(originalBitmap);
            bitmap.recycle();

           filtersListFragment.displayThumbnail(originalBitmap);
        }

    }
}
